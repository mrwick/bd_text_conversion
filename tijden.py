from pathlib import Path
import json

notities = Path("./lopen.txt").read_text().split("\n")

lopen = dict()
huidig_datum = None
teller = 0


def lopen_invullen():
    huidig = lopen[huidig_datum]

    tijd = notitie.split()
    tijd_len = len(tijd)
    # print(f"tijd: {tijd}")
    # print(f"teller: {teller}")
    # print(f"huidig: {huidig}")
    tijd_minuut = int(tijd[0])
    tijd_second = int(tijd[1])
    tijd_notitie = None
    if tijd_len > 2:
        tijd_notitie = " ".join(tijd[2: tijd_len])

    def deze_invullen(sleutel):
        deze = huidig[sleutel]
        deze["teller"] = teller
        deze["minuut"] = tijd_minuut
        deze["second"] = tijd_second
        deze["notitie"] = tijd_notitie

    if teller == 1:
        deze_invullen("totaal")
    elif teller == 2:
        deze_invullen("park")
    elif teller == 3:
        deze_invullen("eerste")
    elif teller == 4:
        deze_invullen("tweede")
    elif teller == 5:
        deze_invullen("derde")
    elif teller == 6:
        deze_invullen("thuis")
    elif teller == 7:
        deze_invullen("douche")
    else:
        deze_invullen("andere")


for notitie in notities:
    teller += 1
    if notitie.startswith("2022"):
        teller = 0
        huidig_datum = notitie
        lopen[huidig_datum] = dict(
            totaal=dict(),
            park=dict(),
            eerste=dict(),
            tweede=dict(),
            derde=dict(),
            thuis=dict(),
            douche=dict(),
            andere=dict(),
        )
        continue
    if notitie:
        lopen_invullen()

print(f"{json.dumps(lopen)}")
