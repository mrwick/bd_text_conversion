# bd_text_conversion


## Description
converting text from notes taken on phone to csv/json/whatever

## Usage
so far just `python3.11 tijden.py`
expects a file named lopen.txt with entries like so
```
date
time to park
time first round
time second round
time third round
time to home
time to shower
```
